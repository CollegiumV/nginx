#!/bin/sh -e
# Lint
echo "Linting role..."
yamllint ../ || true
# Syntax check
echo "Checking role syntax..."
ansible-galaxy install -r requirements.yml
ansible-playbook --syntax-check all.yml
# Ansible Playbook
echo "Running role..."
ansible-playbook all.yml
echo "Confirming http works"
curl -s "http://centos7:80/" -H "Host: example.com" | grep "test success"
curl -s "http://voidlinux:80/" -H "Host: example.com" | grep "test success"
